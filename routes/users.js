var express = require('express');
var router = express.Router();
var database = require('../custom_modules/database');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var uuidv4 = require('uuid/v4');

var REQUEST_TYPE = {
	UPDATE_USER: 'update',
	GET_USER: 'get-one',
	GET_CURRENT_USER: 'get-current',
	GET_ALL_USERS: 'get-all',
	GET_MENTOR: 'get-mentor',
	FIRST: 'first-time',
	UPLOAD_PROFILE_PICTURE: 'profile-pic-upload'
}

/* GET users listing. */
router.get('/users', function(req, res, next) {
  res.send('Failed to log in');
});

router.get('/usersapi', function(req, res, next){
	var reqType = req.body.reqType;

	switch(reqType){	
			case REQUEST_TYPE.GET_USER:
				database.getUser(req.body.id, function(err, results){
					if(err){
						console.log('Error!');
						res.sendStatus(500);
					} else {
						res.json(results[0]);
					}
				});
				break;

				case REQUEST_TYPE.GET_CURRENT_USER:
					var authenticated = req.isAuthenticated();
					if(authenticated){
						res.json(req.user);
					} else {
						res.sendStatus(401);
					}
				break;

				case REQUEST_TYPE.GET_ALL_USERS:
				database.getUsers(function(err, results){
					if(err){
						console.log('Error!');
						res.sendStatus(500);
					} else {
						res.json(results);
					}
				});
				break;
		}
});

router.post('/usersapi', function(req, res, next) {
	var reqType = req.body.reqType;
	switch(reqType){
		case REQUEST_TYPE.UPDATE_USER:
			if(!req.isAuthenticated() || (req.body.id != req.user.id)){
				res.sendStatus(401);
				return false;
			}
			database.updateUser(req.body.id, req.body.key, req.body.value, function(err, oldVal){
	  		if(err){
					console.log('Error!!');
					res.sendStatus(500);
				}
	  		else{
					console.log('User successfully updated');
			    }
		  		res.sendStatus(200);
  			});
				break;
			
			case REQUEST_TYPE.GET_USER:
				database.getUser(req.body.id, function(err, results){
					if(err){
						console.log('Error!');
						res.sendStatus(500);
					} else {
						res.json(results[0]);
					}
				});
				break;

				case REQUEST_TYPE.GET_CURRENT_USER:
					var authenticated = req.isAuthenticated();
					if(authenticated){
						res.json(req.user);
					} else {
						res.sendStatus(401);
					}
				break;

				case REQUEST_TYPE.GET_ALL_USERS:
				database.getUsers(function(err, results){
					if(err){
						console.log('Error!');
						res.sendStatus(500);
					} else {
						res.json(results);
					}
				});
				break;

				case REQUEST_TYPE.FIRST:
			database.updateFirstTimeUser(req.user.id, req.body.nickname, function(err){
	  		if(err){
					console.log('Error!!');
					res.sendStatus(500);
				}
	  		else
					console.log('User successfully updated');
		  			res.redirect('/index');
  			});
				break;
			
		}
});

router.post('/mentorsapi', function(req, res, next) {
	var reqType = req.body.reqType;
	
	switch(reqType){
		case REQUEST_TYPE.GET_MENTOR:
			database.getMentor(req.body.id, function(err, results){
	  		if(err){
					console.log('Error!!');
					res.sendStatus(500);
				}
	  		else
					res.json(results[0]);
  			});
				break;
		}
});

router.post('/profileuploadapi', function(req, res, next) {

	switch(req.body.reqType){
		case 'delete':
			database.deleteUploadedFile(req.body.filename, function(err){
				if(!err){ 
					//Remove user uploaded avatar when it is no longer in use.
					var dir = path.join(__dirname, '../public/images/userupload/' + req.body.filename);
					fs.unlink(dir, function(err){
						if(err){
							console.log("Error with removing old avatar from system." + err);
							return res.sendStatus(500);
						} else {
							console.log("Successfully removed old avatar file: " +
							 dir + ", from system.");
							 return res.sendStatus(200);
						}
					});
				}
				else 
					return res.sendStatus(500);
			});
		break;
		default:
			uploadImage();
		break;
	}

	function uploadImage(){
		var form = new formidable.IncomingForm();
		form.multiples = false;
		form.uploadDir = path.join(__dirname, '../public/images/userupload');
		var uuid = 'user-upload' + uuidv4();
		
		form.parse(req, function(err, fields, files){
			var file = files.file;

			if(!file) return res.json({success: 0, message: 'No file selected!'});
			var extension = path.extname(file.name);
			if(!extension){
				console.log("Uploaded file has no extension, error!");
				return res.json({success: 0, message: 'Uploaded file has no extension, error!'});
			} else if(extension.indexOf('jpg') == -1 && extension.indexOf('png') == -1){
				console.log("Uploaded file is not of the right extension. Please upload an image in the format of .png or .jpg");
				return res.json({success: 0, message: 'Uploaded file is not of the right extension, Pleas upload an image in the formart of .png or .jpg'});
			}
			fs.rename(file.path, form.uploadDir + '/' + uuid + path.extname(file.name));
			database.registerUploadedFile(req.user.id, uuid + path.extname(file.name), function(err){
				if(err) {
					console.log('Error with registering uploaded file into database');
					res.sendStatus(500);
				} 
			});
			var result = {
				success: 1,
				url: '/images/userupload/' + uuid + path.extname(files.file.name)
			}
			return res.json(result);
		});
	}
});

router.get('/userslist', function(req, res, next) {
  database.getUsers(function(err, results){
	  if(err){
		  console.log('Error!!');
		  res.sendStatus(500);
		}
	  else{
		  res.json(results);
		}
  });
});

router.get('/welcome', function(req, res, next) {

	var authenticated = req.isAuthenticated();
	if(!authenticated){
		res.render('authenticationError');
	} else {
		if(req.user.firstTime == 0)
			res.render('index', {
		})
		else{
			var nickname = req.user.nickname;
			res.render('welcome', {
				nickname: nickname,
				authenticated: authenticated
			});
		}
	}
});
module.exports = router;
